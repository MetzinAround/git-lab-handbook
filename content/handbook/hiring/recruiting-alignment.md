---
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter   | Candidate Experience Specialist    |
|--------------------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn    | Michelle Jubrey  |
| Executive          | Zach Choquette   | Michelle Jubrey  |
| Enterprise Sales, NA | Kevin Rodrigues |Fernando Khubeir |
| Enterprise Sales, EMEA | Kannwal Matharu | Lerato Thipe |
| Commercial Sales, AMER | Marcus Carter | Michelle Jubrey |
| Commercial Sales, AMER | Hannah Stewart  | Fernando Khubeir |
| Commercial Sales, EMEA | Ben Cowdry | Lerato Thipe |
| Channel Sales, EMEA | Kanwal Matharu  | Lerato Thipe |
| Field Operations, US/EMEA | Kelsey Hart  | Fernando Khubeir |
| Customer Success, EMEA | Joanna Tourne | Lerato Thipe |
| Customer Success, NA | Barbara Dinoff |  Fernando Khubeir |
| All Sales, APAC | Yas Priatna  | Lerato Thipe |
| Marketing, Global | Steph Sarff | Michelle Jubrey |
| Marketing, BDR/SDR (Global)| Caroline Rebello |  Alice Crosbie |
| G&A, Legal | Jenna VanZutphen | Fernando Khubeir |
| G&A, Accounting, People | Jenna VanZutphen | Fernando Khubeir |
| G&A, IT | Jenna VanZutphen | Fernando Khubier |
| Development | Mark Deubel & Sara Currie | Alice Crosbie |
| Quality | Riley Smith |  Alice Crosbie  |
| Infrastructure   | Michelle A. Kemp | Alice Crosbie  |
| Support | Joanna Michniewicz  |  Alice Crosbie |
| Security | Holly Nesselroad | Michelle Jubrey |
| Incubation | Holly Nesselroad  | Michelle Jubrey |
| Product Management  | Holly Nesselroad | Michelle Jubrey |
| UX  | Riley Smith | Alice Crosbie  |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails.

## Talent Acquisition Leader Alignment

| Department                    | Leader      |
|--------------------------|-----------------|
| Talent Acquisition         | Jess Dallmar |
| Talent Brand | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (EMEA and APAC Sales) | Jake Foster |
| Talent Acquisition (Marketing) | Steph Sarff + Jake Foster |
| Talent Acquisition (G&A) | Steph Sarff + Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Talent Acquisition (R&D: Customer Support & Development) | Ursela Knezevic |
| Talent Acquisition (R&D: Infrastructure/Quality, Security, Product/UX, Incubation) | Ursela Knezevic |
| Talent Acquisition (Executive) | Rich Kahn |
| Enablement | Marissa Ferber |
| Candidate Experience | Ale Ayala/Marissa Ferber |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
